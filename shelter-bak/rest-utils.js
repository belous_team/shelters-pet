const uuid = require('uuid-random');

class HttpCodeError extends Error {
    constructor(code, message, details = {}) {
        super(message);
        this.code = code;
        this.details = details;
    }
    getCode() {
        return this.code;
    }
    getDetails() {
        return this.details;
    }
}
function writeResponse(res, payload, req) {
    let clientId = undefined;
    if (req && req.headers['x-client-id']) {
        clientId = req.headers['x-client-id'];
    } else {
        clientId = uuid();
    }
    let code = 200;
    let details = {};
    res.writeHead(code, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'X-Client-ID': clientId});
    res.end(JSON.stringify({data: payload, status:{code, message: 'successful', details}}));
}
function writeResponseError(res, err, req) {
    let clientId = undefined;
    if (req && req.headers['x-client-id']) {
        clientId = req.headers['x-client-id'];
    } else {
        clientId = uuid();
    }
    let code = 500;
    let details = {};
    if(err && typeof err.getCode === 'function') {
        code = err.getCode();
        details = err.getDetails();
    }
    res.writeHead(code, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'X-Client-ID': clientId});
    res.end(JSON.stringify({status: code, details, message: err.message}));
}

module.exports = {HttpCodeError, writeResponseError, writeResponse};
