/*==============================================================*/
/* dbschema version 1                                           */
/*==============================================================*/
drop table if exists dbschema;
create table dbschema (
    version int not null
);
insert into dbschema ("version")
values (7);


CREATE TABLE IF NOT EXISTS public.ccinfo (
	id uuid NOT NULL,
	"name" varchar NOT NULL UNIQUE,
	creation_date varchar NOT NULL,
	db_connections varchar[] NOT NULL,
	db_name varchar NOT NULL,
	db_username varchar NOT NULL,
	db_password varchar NOT NULL,
	los_type int2 NOT NULL, -- large object storage type 0 - file, 1 - postgres db, 2 - nexus upload service
	los_path varchar NOT NULL, -- large object storage path (file storage)
	los_db_host varchar NOT NULL, -- db info for large object storage
	los_db_port int2 NOT NULL,
	los_db_name varchar NOT NULL,
	los_db_username varchar NOT NULL,
	los_db_password varchar NOT NULL,
	los_nexus_url varchar,
	los_nexus_bucket varchar,
	los_nexus_apikey varchar,
	es_info TEXT,
	options jsonb,
	CONSTRAINT ccinfo_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.logger (
    "service"   varchar not null,
	"name"      varchar not null,
	"level"     varchar not null,
    unique      ("service", "name")
);
delete from public.logger;
insert into public.logger ("service", "name", "level")
VALUES
(	'ucsx', 'default',	'debug'),
(	'ucsx', 'Service.ApplicationContext',	'info'),
(	'ucsx', 'Service.ApplicationCache',	'info'),
(	'ucsx', 'Service.Metrics',	'info'),
(	'ucsx', 'REST.Metrics.Middleware',	'info'),
(	'ucsx', 'Application.Metrics',	'debug'),
(	'ucsx', 'Service.LogRotationService.Handle',	'info');

create table if not exists public.logger_mask(
    "service"   varchar not null,
	"probeType"	varchar not null,
	"probeArg" 	varchar not null,
	"maskType" 	varchar not null
);

delete from public.logger_mask;
insert into public.logger_mask ("service", "probeType", "probeArg", "maskType")
VALUES
(	'ucsx', 'prop',	'password,client_secret,content,Content,structuredtext,structuredText,StructuredText,attributes,primaryattributes,xapikey', 'full'),
(	'ucsx', 'attr',	'StructuredText', 'full'),
(	'ucsx', 'attr',	'PhoneNumber', 'phone'),
(	'ucsx', 'prop',	'phoneNumber,phoneNumberE164,e164,PhoneNumber,phonenumber,phoneUserNumber,phoneusernumber,PhoneUserNumber,userValue', 'phone');

CREATE TABLE if not exists public.parameter (
    "service"       varchar not null,
	"name"          varchar not null,
	"value"         varchar not null,
	"description"   varchar not null,
    unique          ("service", "name")
);

delete from public.parameter;
INSERT INTO public.parameter ("service", "name", "value", "description")
VALUES
(	'ucsx',     'log.rotation.interval',			'0',	    	'[0] How ofter logrotate will be called, ms, 0 - disable log rotation'),
(	'ucsx',     'log.rotation.frequency',			'hourly',		'[hourly, daily, weekly, monthly, yearly] Frequency of rotation'),
(	'ucsx',     'log.rotation.count',				'7',			'[0, 1, 3, 14] Log files are rotated count times before being removed. 0 means old versions are removed rather than rotated.'),
(	'ucsx',     'log.rotation.maxsize', 			'100M', 		'[100, 100k, 100M, 100G] Log files are rotated when they grow bigger than size bytes, k - kilobytes, M - megabytes, G - gigabytes'),
(	'ucsx',     'log.rotation.compress', 			'true',			'[true, false] Old versions of log files are compressed with gzip'),
(	'ucsx',     'log.rotation.delaycompress', 		'true', 		'[true, false] Postpone compression of the previous log file to the next rotation cycle.'),
(	'ucsx',     'log.suppress.contexts',
		'Scheduler.Datasync.Metric,Scheduler.Metrics.Queue,Scheduler.Metrics.SQL,Scheduler.Metrics.CPU,Scheduler.Metrics.LoopDelay,Scheduler.CheckPools,Scheduler.CheckPools.Los,Once.OverloadProtection,Scheduler.RemoveExpired,LogConfigurationService,Once.TTL.Scheduling,ConfigurationProvider',
		'[ctx1, ctx2, ctx3] Suppress TRACE, DEBUG, INFO messages from globals context'),
(	'ucsx',     'log.suppress.urls',
		'/metrics,/ucs/v3/healthcheck,/favicon.ico,/ucs/v3/config/healthcheck',
		'[url1, url2, url3] Suppress TRACE, DEBUG, INFO messages where request context url match with this list'),

(	'cddsx',     'log.rotation.interval',			'0',	    	'[0] How ofter logrotate will be called, ms, 0 - disable log rotation'),
(	'cddsx',     'log.rotation.frequency',			'hourly',		'[hourly, daily, weekly, monthly, yearly] Frequency of rotation'),
(	'cddsx',     'log.rotation.count',				'7',			'[0, 1, 3, 14] Log files are rotated count times before being removed. 0 means old versions are removed rather than rotated.'),
(	'cddsx',     'log.rotation.maxsize', 			'100M', 		'[100, 100k, 100M, 100G] Log files are rotated when they grow bigger than size bytes, k - kilobytes, M - megabytes, G - gigabytes'),
(	'cddsx',     'log.rotation.compress', 			'true',			'[true, false] Old versions of log files are compressed with gzip'),
(	'cddsx',     'log.rotation.delaycompress', 		'true', 		'[true, false] Postpone compression of the previous log file to the next rotation cycle.'),
(	'cddsx',     'log.suppress.contexts',
		'LogConfigurationService,ConfigurationProvider',
		'[ctx1, ctx2, ctx3] Suppress TRACE, DEBUG, INFO messages from globals context'),
(	'cddsx',     'log.suppress.urls',
		'/metrics,/ucs/v3/healthcheck,/favicon.ico,/ucs/v3/config/healthcheck',
		'[url1, url2, url3] Suppress TRACE, DEBUG, INFO messages where request context url match with this list'),

(	'cddsx-ui',     'log.rotation.interval',			'0',	    	'[0] How ofter logrotate will be called, ms, 0 - disable log rotation'),
(	'cddsx-ui',     'log.rotation.frequency',			'hourly',		'[hourly, daily, weekly, monthly, yearly] Frequency of rotation'),
(	'cddsx-ui',     'log.rotation.count',				'7',			'[0, 1, 3, 14] Log files are rotated count times before being removed. 0 means old versions are removed rather than rotated.'),
(	'cddsx-ui',     'log.rotation.maxsize', 			'100M', 		'[100, 100k, 100M, 100G] Log files are rotated when they grow bigger than size bytes, k - kilobytes, M - megabytes, G - gigabytes'),
(	'cddsx-ui',     'log.rotation.compress', 			'true',			'[true, false] Old versions of log files are compressed with gzip'),
(	'cddsx-ui',     'log.rotation.delaycompress', 		'true', 		'[true, false] Postpone compression of the previous log file to the next rotation cycle.'),
(	'cddsx-ui',     'log.suppress.contexts',
		'LogConfigurationService,ConfigurationProvider',
		'[ctx1, ctx2, ctx3] Suppress TRACE, DEBUG, INFO messages from globals context'),
(	'cddsx-ui',     'log.suppress.urls',
		'/healthcheck,/favicon.ico',
		'[url1, url2, url3] Suppress TRACE, DEBUG, INFO messages where request context url match with this list')
;


CREATE TABLE IF NOT EXISTS public.ttl_running (
    id           uuid PRIMARY KEY, -- ccID
    last_running TIMESTAMP,        -- when ttl script was launched
    last_monitor TIMESTAMP,        -- shows that ttl-script is running
    last_finish  TIMESTAMP         -- when ttl-script was finished
);

CREATE TABLE IF NOT EXISTS public.datasync_state (
      id           uuid PRIMARY KEY, -- ccID
      modified    timestamptz default now(),
      state       varchar(20),
      message     text,
      c_from_time varchar(30),
      c_from_id   varchar(20),
      i_from_time varchar(30),
      i_from_id   varchar(20),
      c_batches   integer, --number of finished batches
      i_batches   integer,
      c_time      integer, --number of second for finished batches
      i_time      integer,
      c_update    timestamp,-- last batch update
      i_update    timestamp
);

CREATE TABLE IF NOT EXISTS public.certificate (
    ccId uuid NOT NULL,
    name varchar NOT NULL,
    serialNumber varchar NOT NULL,
    issuedBy varchar NOT NULL,
    issuedTo varchar NOT NULL,
    validNotBefore timestamp with time zone NOT NULL,
    validNotAfter timestamp with time zone NOT NULL,
    body varchar NOT NULL,
    CONSTRAINT certificate_ccid_name_pk PRIMARY KEY (ccId, name)
);

CREATE TABLE IF NOT EXISTS public.reindex_state (
    id uuid NOT NULL,
    ccId uuid NOT NULL,
    startDate timestamptz default NOW(),
    parameters jsonb,
    contact_processed integer default 0,
    interaction_processed integer default 0,
    standardresponse_processed integer default 0,
    errors jsonb default '{}'::jsonb,
    CONSTRAINT reindex_state_pk PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS public.reindex_history (
    id uuid NOT NULL,
    ccId uuid NOT NULL,
    startDate timestamptz NOT NULL,
    endDate timestamptz NOT NULL,
    parameters jsonb,
    results jsonb,
    CONSTRAINT reindex_history_pk PRIMARY KEY (id)
);
create index if not exists reindex_history_ccid on public.reindex_history (ccId);

/*** CDDSX ***/
create sequence if not exists public.jobs_sequence as bigint
increment by 1 start with 1 cache 3 no cycle;

CREATE TABLE IF NOT EXISTS public.jobs (
    id bigint primary key,
    ccid uuid not null references ccinfo(id) on delete cascade,
    source varchar(10),
    title varchar(256) not null,
    created timestamptz default now(),
    description varchar(256),
    time_frame jsonb,
    parameters jsonb,
    scheduler jsonb,
    active boolean default false,
    suspended boolean default false,
    in_progress boolean default false,
    duration interval,
    last_running timestamptz,
    last_monitoring timestamptz,
    last_finish timestamptz,
    next_run timestamptz
);

CREATE TABLE IF NOT EXISTS public.lock_job (
   ccid uuid not null unique references ccinfo(id) on DELETE restrict,
   job_id bigint not null unique references jobs(id) ON DELETE RESTRICT,
   node varchar(256)
);

CREATE TABLE IF NOT EXISTS public.job_history (
    id bigint primary key,
    job_id bigint references jobs(id) on delete cascade, -- if we need delete jobs and history
    run_date timestamptz,
    duration interval,
    status numeric(1), -- 0 - running, 1 - finished successfully, 2 - canceled, 3 - failed
    failed_reason varchar(1024),
    progress integer,
    rescheduled_count integer,
    update_date timestamptz
);

CREATE TABLE IF NOT EXISTS export_files (
    path varchar(1024) primary key,
    history_id bigint references job_history(id) on delete cascade,
    upload_date timestamptz,
    size integer,
    active boolean
);

with job as (
    select * from jobs
), last as (
    select j.id, max(h.id) historyId from job_history h, job j where h.job_id = j.id and h.status != 0 group by j.id
), history as (
    select job_id,
           ('{"id":' || job_history.id || ',"createdDate":"' || to_char(run_date, 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') || '","status":"' ||
            case
            when status = 0 then 'RUNNING'
            when status = 1 then 'FINISHED'
            when status = 2 then 'CANCELED'
            when status = 3 then 'FAILED'
            end || '","parentId":' || job_id || ',"progress":' || progress || '}')::json as last_run_json
    from job_history, last where job_history.id = historyId
)
select
    j.id, j.source, j.title, j.created as "createdDate", j.time_frame as "timeFrame", j.description, j.scheduler as "schedulerParam",
    (case when j.next_run is not null then '{"startDate":"' || to_char(j.next_run, 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') || '"}' else null end)::json as "nextRun",
    j.parameters as "jobParams",
    h.last_run_json as "lastRun", j.suspended
from job j
         left join history h on h.job_id = j.id;

with job as (
    select * from jobs
), last as (
    select j.id, max(h.id) historyId from job_history h, job j where h.job_id = j.id and h.status != 0 group by j.id
), history as (
    select job_id,
           ('{"id":' || job_history.id || ',"createdDate":"' || to_char(run_date, 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') || '","status":"' ||
            case
            when status = 0 then 'RUNNING'
            when status = 1 then 'FINISHED'
            when status = 2 then 'CANCELED'
            when status = 3 then 'FAILED'
            end || '","parentId":' || job_id || ',"progress":' || progress || '}')::json as last_run_json
    from job_history, last where job_history.id = historyId
)
select
    j.id, j.source, j.title, j.created as "createdDate", j.time_frame as "timeFrame", j.description, j.scheduler as "schedulerParam",
    (case when j.next_run is not null then '{"startDate":"' || to_char(j.next_run, 'YYYY-MM-DD"T"HH24:MI:SS.MS"Z"') || '"}' else null end)::json as "nextRun",
    j.parameters as "jobParams",
    h.last_run_json as "lastRun", j.suspended
from job j
         left join history h on h.job_id = j.id;
