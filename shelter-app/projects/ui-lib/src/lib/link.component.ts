import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'lib-link',
  template: `<div (click)="onCLick()" class="link-container">
    <img src="{{icoUri}}" *ngIf="icoUri" style="width: 32px; height: 32px;margin-left: 4px;">
    <i class="{{classes}}" style="font-size: 32px;margin-left: 4px;" *ngIf="faIcon"></i>
    <mat-icon style="font-size: 32px;margin-left: 4px;" *ngIf="matIcon">{{matIcon}}</mat-icon>
    <div style="margin: 8px 8px 8px 8px;">
      <div class="content">
        <ng-content></ng-content>
      </div>
    </div>
  </div>`,
  styles: [
    '.link-container {display: flex;flex-direction: row;align-items: center;}',
    '.link-container:hover {cursor: pointer;}',
    '.content {display: block;}'
  ]
})
export class LinkComponent implements OnInit {
  @Input() href: string;
  @Input() matIcon: string;
  @Input() faIcon: string;
  icoUri: string;

  constructor(private router: Router) { }

  ngOnInit(): void {
    if (!this.matIcon && !this.faIcon) {
      if (this.href.startsWith('tel:')) {
        this.faIcon = 'phone';
      } else if (this.href.startsWith('mailto:')) {
        this.faIcon = 'envelope';
      } else if (this.href.startsWith('http:') || this.href.startsWith('https:') ){
        const url = new URL(this.href);
        this.icoUri = url.protocol + '//' + url.host + '/favicon.ico';
      }
    }
  }
  get classes(): any {
    const cssClasses = {
      fa: true
    };
    cssClasses['fa-' + this.faIcon] = true;
    return cssClasses;
  }
  onCLick(): void {
    if (this.href.startsWith('/')) {
      this.router.navigate([this.href]);
    } else {
      window.open(this.href, '_blank');
    }
  }
}
