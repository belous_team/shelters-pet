import {AfterViewInit, Component, ElementRef, forwardRef, Input, OnInit} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';

export const CHECKBOX_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CheckboxControlComponent),
  multi: true
};

@Component({
  selector: 'lib-checkbox-control',
  templateUrl: './checkbox-control.component.html',
  styleUrls: ['./checkbox-control.component.scss'],
  providers: [CHECKBOX_VALUE_ACCESSOR]
})
export class CheckboxControlComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  @Input() options: string[];
  @Input() default: string[];
  values: string[];
  onChange: (_: any) => {};
  onTouch: () => {};
  isDisabled: boolean;

  constructor(private refElement: ElementRef) {
    console.log(refElement);
  }

  ngAfterViewInit(): void {
  }

  writeValue(obj: any): void {
    if (obj) {
      const newValue = [];
      Object.values(obj).forEach(v => {
        console.log(v);
        newValue.push(v);
      });
      this.values = newValue;
    }
  }
  registerOnChange(fn: (_: any) => {}): void {
    this.onChange = ((_: any) => {
      if (_ && _.target) {
        const ch = _.target.checked;
        const v = _.target.value;
        if (ch && !this.values.includes(v)) {
          this.values.push(v);
        } else {
          this.values = this.values.filter(k => k !== v);
        }
      }
      fn(this.values);
    }) as (_: any) => {};
  }
  registerOnTouched(fn: any): void {
      this.onTouch = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  ngOnInit(): void {
    this.values = this.default;
    if (!this.values) {
      this.values = [];
    }
  }

}
